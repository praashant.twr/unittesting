import csv
from utilities import *

def player_economy(file_location):
    economy_dict = {}
    try:
        with open(file_location, 'r') as delivery_file:
            delivery_reader = csv.DictReader(delivery_file)
            season_id = find_match_id_for_season('mock_matches.csv', '2010')
            for delivery in delivery_reader:
                if delivery['match_id'] in season_id:
                    bowler = delivery['bowler']
                    runs = delivery['total_runs']
                    bye_runs = delivery['bye_runs']
                    legbye_runs = delivery['legbye_runs']

                    if bowler in economy_dict:
                        economy_dict[bowler]['runs'] += int(runs) - int(bye_runs) - int(legbye_runs)
                        economy_dict[bowler]['balls'] += 1
                    else:
                        economy_dict[bowler] = {'runs': int(runs) - int(bye_runs) - int(legbye_runs),'balls': 1}
        return economy_dict
    except FileNotFoundError:
        return 'file not found'

    except KeyError:
        return 'wrong file imported'
    
    except TypeError:
        return 'wrong input format'

# print(player_economy('mock_deliveries.csv'))