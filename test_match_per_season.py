import unittest
from matches_per_season import match_per_season

class TestImport(unittest.TestCase):
    def test_correct_import(self):
        self.assertEqual(match_per_season('mock_matches.csv'), {'2017': 3, '2010': 2})
        self.assertEqual(match_per_season('some_wrong_location'), 'file not found')
        self.assertEqual(match_per_season('mock_deliveries.csv'), 'wrong file imported')
        self.assertEqual(match_per_season({}), 'wrong input format')
