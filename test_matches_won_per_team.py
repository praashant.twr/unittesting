import unittest
from matches_won_per_team import matches_per_team

class TestMatchWinner(unittest.TestCase):

    def test_match_winners(self):
        expected_output = {
            'Sunrisers Hyderabad': {'2017': 1},
            'Rising Pune Supergiant': {'2017': 1},
            'Kolkata Knight Riders': {'2017': 1},
            'Mumbai Indians': {'2010': 1},
            'Delhi Daredevils': {'2010': 1}
            }

        output = matches_per_team('mock_matches.csv')
        self.assertEqual(output, expected_output)

        output = matches_per_team('invalid_name')
        self.assertEqual(output, 'file not found')
        
        output = matches_per_team({})
        self.assertEqual(output, 'wrong input type')
    
        output = matches_per_team('mock_deliveries.csv')
        self.assertEqual(output, 'wrong file imported')


        # with self.assertRaises(FileNotFoundError):
        #     matches_per_team('some_invalid_name')
        
        # with self.assertRaises(TypeError):
        #     matches_per_team({})
        
        # with self.assertRaises(KeyError):
        #     matches_per_team('deliveries.csv')
if __name__ == "__main__":
    unittest.main()