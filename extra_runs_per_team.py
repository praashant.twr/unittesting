import csv
from utilities import find_match_id_for_season

def extra_runs_conceded(file_location):
    try:
        season_id = find_match_id_for_season('mock_matches.csv', '2017')
        extra_run_dict = {}
        with open(file_location, 'r') as deliver_file:
            deliver_reader = csv.DictReader(deliver_file)

            # for id in season_id:
            for delivery in deliver_reader:
                if delivery['match_id'] in season_id:
                    if delivery['bowling_team'] in extra_run_dict:
                        extra_run_dict[delivery['bowling_team']] += int(delivery['extra_runs'])
                    else:
                        extra_run_dict[delivery['bowling_team']] = int(delivery['extra_runs'])
        return extra_run_dict
    except FileNotFoundError:
        return 'file not found'

    except KeyError:
        return 'wrong file imported'
    
    except TypeError:
        return 'wrong input format'


# print(extra_runs_conceded('mock_deliveries.csv'))