import csv

def find_match_id_for_season(file, season):
    match_id = []
    try:
        with open(file, 'r') as match_file:
            match_reader = csv.DictReader(match_file)

            for match in match_reader:
                if match['season'] == season:
                    match_id.append(match['id'])
        return match_id
    except FileNotFoundError:
        return 'file not found'

    except KeyError:
        return 'wrong file imported'
    
    except TypeError:
        return 'wrong input format'

# print(find_match_id_for_season('mock_matches.csv', '2017'))