import csv

def match_per_season(file_location):
    try:
        with open(file_location, 'r') as match_file:
            match_reader = csv.DictReader(match_file)

            match_dict = {}
            for match in match_reader:
                if match['season'] in match_dict:
                    match_dict[match['season']] += 1
                else:
                    match_dict[match['season']] = 1
        return match_dict
    except FileNotFoundError:
        return 'file not found'

    except KeyError:
        return 'wrong file imported'
    
    except TypeError:
        return 'wrong input format'


# print(match_per_season('mock_matches.csv'))
