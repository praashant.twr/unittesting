import unittest
from player_economy import player_economy

class TestEconomy(unittest.TestCase):

    def test_economy(self):
        expected_output = {
            'AD Mascarenhas': {'runs': 13, 'balls': 6},
            'DP Nannes': {'runs': 1, 'balls': 6}
                }
        
        output = player_economy('mock_deliveries.csv')
        self.assertEqual(expected_output, output)

        output = player_economy('invalid_location')
        self.assertEqual(output, 'file not found')

        output = player_economy('mock_matches.csv')
        self.assertEqual(output, 'wrong file imported')

        output = player_economy([])
        self.assertEqual(output, 'wrong input format')
       