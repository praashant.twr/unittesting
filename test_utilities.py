import unittest
from utilities import find_match_id_for_season

class TestUtilities(unittest.TestCase):

    def test_match_id(self):
        self.assertEqual(find_match_id_for_season('mock_matches.csv', '2017'), ['1', '2', '3'])
        self.assertEqual(find_match_id_for_season('mock_matches.csv', '2010'), ['176', '177'])
        self.assertEqual(find_match_id_for_season('some_wrong_location', '2017'), 'file not found')
        self.assertEqual(find_match_id_for_season('mock_deliveries.csv', '2017'), 'wrong file imported')
        self.assertEqual(find_match_id_for_season({}, '2017'), 'wrong input format')

