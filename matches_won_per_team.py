import csv

def matches_per_team(file_location):
    match_dict = {}
    try:
        with open(file_location, 'r') as match_file:
            match_reader = csv.DictReader(match_file)
            for match in match_reader:
                if match['winner'] in match_dict:
                    if match['season'] in match_dict[match['winner']]:
                        match_dict[match['winner']][match['season']] += 1
                    else:
                        match_dict[match['winner']][match['season']] = 1
                else:
                    match_dict[match['winner']] = {match['season']: 1}
        return match_dict
    except FileNotFoundError:
        return 'file not found'
    except TypeError:
        return 'wrong input type'
    except KeyError:
        return 'wrong file imported'

# print(matches_per_team('mock_matches.csv'))