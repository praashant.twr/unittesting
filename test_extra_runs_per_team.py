import unittest
from extra_runs_per_team import *

class TestExtraRuns(unittest.TestCase):

    def test_extra_runs(self):
        expected_output = {
            'Royal Challengers Bangalore': 2,
            'Rising Pune Supergiant': 1,
            'Kolkata Knight Riders': 0
            }

        output = extra_runs_conceded('mock_deliveries.csv')
        self.assertEqual(output, expected_output)

        output = extra_runs_conceded('wrong_location')
        self.assertEqual(output, 'file not found')

        output = extra_runs_conceded('mock_matches.csv')
        self.assertEqual(output, 'wrong file imported')

        output = extra_runs_conceded({})
        self.assertEqual(output, 'wrong input format')
        